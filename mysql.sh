#!/usr/bin/env bash
sudo apt-get update --fix-missing -y && sudo apt-get install -qq mysql-server
DBNAME=demo3
DBUSER=demo3
DBPASSWD=1Q2W3E4r5tzxc@
ROOTPASSWD=1Q2W3E4r5tzxc@
debconf-set-selections <<< "mysql-server mysql-server/root_password password $ROOTPASSWD"
debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $ROOTPASSWD"
readonly MYSQL=which mysql

readonly Q1="CREATE DATABASE IF NOT EXISTS $DBNAME;"
readonly Q2="CREATE USER IF NOT EXISTS '$DBUSER'@'72.16.0.%' IDENTIFIED BY '$DBPASSWD';"
readonly Q3="GRANT ALL ON $DBNAME.* TO '$DBUSER'@'172.16.0.%';"
readonly Q4="FLUSH PRIVILEGES;"
readonly SQL="${Q1}${Q2}${Q3}${Q4}"

$MYSQL -uroot -p$ROOTPASSWD -e "$SQL"

sudo sed -i "s/bind-address.*/bind-address = 0.0.0.0/" /etc/mysql/mysql.conf.d/mysqld.cnf

#mysql -uroot -p$ROOTPASSWD -e "DROP DATABASE $DBNAME;"
#mysql -uroot -p$ROOTPASSWD -e "DROP USER $DBUSER;"

sudo service mysql restart
