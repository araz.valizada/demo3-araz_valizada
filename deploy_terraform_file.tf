provider "aws" {

region = "eu-central-1"

}

resource "aws_vpc" "main" {
  cidr_block = "10.0.0.0/16"
}

resource "aws_subnet" "main" {
vpc_id = aws_vpc.main.id
cidr_block = "10.0.1.0/24"
availability_zone = "eu-central-1a"

}

resource "aws_network_interface" "ens33" {
subnet_id = aws_subnet.main.id
private_ips = ["10.0.1.100"]

}


resource "aws_eip_association" "eip_assoc" {
  instance_id   = aws_instance.app_demo3.id
  allocation_id = aws_eip.eip.id
}

resource "aws_eip" "eip" {
  vpc = true
}

resource "aws_instance" "app_demo3" {

ami = "ami-05f7491af5eef733a"
instance_type = "t3.micro"
vpc_security_group_ids = [aws_security_group.demo3_fw.id]
user_data = <<-EOF
        #! /bin/bash
        sudo apt-get update -y
        sudo apt install openjdk-14-jdk -y
        sudo apt install git -y
        sudo apt install zip -y
        sudo apt install wget -y
        sudo wget https://gitlab.com/thuseynovint/demo_terraform/-/jobs/artifacts/main/download?job=build-job
        sudo unzip /'download?job=build-job'
        sudo java -jar /demo1/target/*.jar
        export MYSQL_PASS="1Q2W3E4r5tzxc@"
        export MYSQL_URL="jdbc:mysql://db:3306/demo3"
        export MYSQL_USER="demo3"
        EOF
tags = {
Name = "demo3_teymur_app"
}

}


resource "aws_instance" "demo3_db" {

ami = "ami-05f7491af5eef733a"
instance_type = "t3.micro"
vpc_security_group_ids = [aws_security_group.demo3_fw.id]
user_data = <<-EOF
        #! /bin/bash
        sudo apt-get update --fix-missing -y && sudo apt-get install -qq mysql-server
        DBNAME=demo3
        DBUSER=demo3
        DBPASSWD=1Q2W3E4r5tzxc@
        ROOTPASSWD=1Q2W3E4r5tzxc@
        debconf-set-selections <<< "mysql-server mysql-server/root_password password $ROOTPASSWD"
        debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $ROOTPASSWD"
        MYSQL=which mysql

        $Q1="CREATE DATABASE IF NOT EXISTS $DBNAME;"
        $Q2="CREATE USER IF NOT EXISTS '$DBUSER'@'10.0.1.%' IDENTIFIED BY '$DBPASSWD';"
        $Q3="GRANT ALL ON $DBNAME.* TO '$DBUSER'@'10.0.1.%';"
        $Q4="FLUSH PRIVILEGES;"
        

        $MYSQL -uroot -p$ROOTPASSWD -e "$Q1"
        $MYSQL -uroot -p$ROOTPASSWD -e "$Q2"
        $MYSQL -uroot -p$ROOTPASSWD -e "$Q3"
        $MYSQL -uroot -p$ROOTPASSWD -e "$Q4"

        sudo sed -i "s/bind-address.*/bind-address = 0.0.0.0/" /etc/mysql/mysql.conf.d/mysqld.cnf
        sudo service mysql restart
        EOF
tags = {
Name = "demo3_teymur_db"
}

}



resource "aws_security_group" "demo3_fw" {
name = "demo3_fw"
description = "Allow TLS inbound traffic"


ingress {
description = "Allow SSH"
from_port =0
to_port = 0
protocol = "-1"
cidr_blocks = ["0.0.0.0/0"]
ipv6_cidr_blocks = ["::/0"]
}

egress {
from_port = 0
to_port = 0
protocol = "-1"
cidr_blocks = ["0.0.0.0/0"]
ipv6_cidr_blocks = ["::/0"]
}
}
